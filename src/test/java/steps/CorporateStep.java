package steps;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.Когда;

import java.util.Map;

import static com.codeborne.selenide.Selenide.$x;

public class CorporateStep {

    @Когда("^заполняет поля в форме - Оставить заявку на корпоративное обучение$")
    public void fillField(Map<String, String> map) {
        map.forEach((element, value) -> {
                    SelenideElement field = $x("//div [@class='form-section']//input [@placeholder ='" + element + "']");
                    field.setValue(value);
                }
        );
    }


}
