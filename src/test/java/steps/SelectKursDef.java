package steps;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.ru.Когда;
import org.junit.jupiter.api.Assertions;

import java.util.Map;

import static com.codeborne.selenide.Selenide.$x;

public class SelectKursDef {

    @And("Вводим данные для записи на курс")
    public void filNewData() {
        SelenideElement nameForm = $x("//span[@class='el-dialog__title']");
        Assertions.assertEquals("Записаться на курс", nameForm.getOwnText(), "Неверная форма");

        SelenideElement field = $x("//label[text()='ФИО']/ancestor::div[@class='el-col el-col-8 el-col-xs-24']//input");
        field.setValue("Nik Nelson");
        Assertions.assertEquals("Nik Nelson", field.getValue(), "Неверно заполнено поле");
    }

    @Когда("^заполняет поля на странице$")
    public void fillField(Map<String, String> map) {
        map.forEach((element, value) -> {
                    SelenideElement field = $x("//label[text()='" + element + "']/ancestor::div[@class='el-col el-col-8 el-col-xs-24']//input");
                    field.setValue(value);
                }
        );
    }

}
