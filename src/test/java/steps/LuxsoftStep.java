package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

public class LuxsoftStep {

    private final SelenideElement textBoxInput = $x("//input[@type='text'][@class='search-main']");
    private final SelenideElement kurs = $x("//a[text()='Java'][@class='search-phrase__link']");
    private final SelenideElement level = $x("//a[@class='title dropdown-link']");
    private final SelenideElement levelJunior = $x("//a[text()='Junior']");
    private final SelenideElement chooseKurs = $x("//a[text()='Введение в Apache Maven']");
    private final SelenideElement buttonЗаписаться_курс = $x("//div[@class='course-detail__offer']//span[text()='Записаться на курс']");

    @And("^пользователь выбирает вкладку \"(.*?)\"$")
    public void chooseTab(String tab) {
        SelenideElement commonTab = $x("//a[text()='" + tab + "']");
        commonTab.should(Condition.visible).click();
    }

    @When("Открыть вкладку Корпоративное обучение")
    public void корпоративное_обучение() {
        textBoxInput.click();
        textBoxInput.setValue("Java");
        textBoxInput.sendKeys(Keys.ENTER);
    }

    @And("Выбор курса")
    public void selectKurs() {
        kurs.scrollIntoView(false).click();
        level.hover().click();
        levelJunior.hover().click();
        chooseKurs.scrollIntoView(false).hover().click();
        buttonЗаписаться_курс.click();
    }

    protected void filNewData() {
        SelenideElement field = $x("//label[text()='ФИО']/ancestor::div[@class='el-col el-col-8 el-col-xs-24']//input");
        field.setValue("Nik Nelson");
        Assertions.assertEquals("Nik Nelson", field.getValue(), "Неверно заполнено поле");

    }

    @And("ВВод данных {string}")
    public void вводДанных(String arg0) {
    }

}
