package steps;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;

public class BeforeStep {
    protected static WebDriver driver;
    private static final int TIMOUT = 60;

    @Given("Открываем сайт {string}")
    public void openWebSite(String url) {
        Configuration.timeout = 60000;
        Selenide.open(url);
    }
}
